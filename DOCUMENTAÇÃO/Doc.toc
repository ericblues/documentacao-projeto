\changetocdepth {4}
\select@language {brazil}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {part}{\partnumberline {I}\MakeTextUppercase {Vis\IeC {\~a}o Geral}}{12}{part.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Introdu\IeC {\c c}\IeC {\~a}o}}{13}{chapter.1}
\contentsline {section}{\numberline {1.1}Problema}{14}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivos}{15}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Objetivo Geral}{15}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Objetivos Espec\IeC {\'\i }ficos}{15}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Justificativa}{15}{section.1.3}
\contentsline {section}{\numberline {1.4}Aspectos Metodol\IeC {\'o}gicos}{16}{section.1.4}
\contentsline {section}{\numberline {1.5}Aporte Te\IeC {\'o}rico}{16}{section.1.5}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Fundamenta\IeC {\c c}\IeC {\~a}o Te\IeC {\'o}rica}}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Cloud Firestore}{17}{section.2.1}
\contentsline {section}{\numberline {2.2}Trabalhos Relacionados}{17}{section.2.2}
\contentsline {part}{\partnumberline {II}\MakeTextUppercase {An\IeC {\'a}lise}}{18}{part.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Requisitos}}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Requisitos Funcionais}{19}{section.3.1}
\contentsline {section}{\numberline {3.2}N\IeC {\~a}o Funcionais}{24}{section.3.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Casos de Uso}}{27}{chapter.4}
\contentsline {section}{\numberline {4.1}Lista de Casos de Uso}{27}{section.4.1}
\contentsline {section}{\numberline {4.2}Diagrama de casos de uso}{31}{section.4.2}
\contentsline {section}{\numberline {4.3}Casos de Uso Textual}{41}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Caso de uso textual - Cadastrar Rep\IeC {\'u}blica}{41}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Caso de uso textual - Editar Rep\IeC {\'u}blica}{42}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Caso de uso textual - Pesquisar Rep\IeC {\'u}blica}{43}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Caso de uso textual - Solicitar Vaga}{44}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}Caso de uso textual - Aceitar Solicita\IeC {\c c}\IeC {\~a}o}{45}{subsection.4.3.5}
\contentsline {subsection}{\numberline {4.3.6}Caso de uso textual - Editar Informa\IeC {\c c}\IeC {\~o}es Pessoais}{46}{subsection.4.3.6}
\contentsline {subsection}{\numberline {4.3.7}Caso de uso textual - Cadastrar Despesa}{47}{subsection.4.3.7}
\contentsline {subsection}{\numberline {4.3.8}Caso de uso textual - Editar Despesa}{48}{subsection.4.3.8}
\contentsline {subsection}{\numberline {4.3.9}Caso de uso textual - Excluir Despesa}{49}{subsection.4.3.9}
\contentsline {subsection}{\numberline {4.3.10}Caso de uso textual - Atribuir Despesa}{50}{subsection.4.3.10}
\contentsline {subsection}{\numberline {4.3.11}Caso de uso textual - Editar Atribui\IeC {\c c}\IeC {\~a}o Despesa}{51}{subsection.4.3.11}
\contentsline {subsection}{\numberline {4.3.12}Caso de uso textual - Excluir Atribui\IeC {\c c}\IeC {\~a}o Despesa}{53}{subsection.4.3.12}
\contentsline {subsection}{\numberline {4.3.13}Caso de uso textual - Pesquisar Despesa}{55}{subsection.4.3.13}
\contentsline {subsection}{\numberline {4.3.14}Caso de uso textual - Pagar Despesa}{56}{subsection.4.3.14}
\contentsline {subsection}{\numberline {4.3.15}Caso de uso textual - Consultar Recibo}{58}{subsection.4.3.15}
\contentsline {subsection}{\numberline {4.3.16}Caso de uso textual - Pesquisar Despesa Usu\IeC {\'a}rio}{60}{subsection.4.3.16}
\contentsline {subsection}{\numberline {4.3.17}Caso de uso textual - Gerar Relat\IeC {\'o}rio de Despesas}{62}{subsection.4.3.17}
\contentsline {subsection}{\numberline {4.3.18}Caso de uso textual - Dep\IeC {\'o}sito Caixa}{63}{subsection.4.3.18}
\contentsline {subsection}{\numberline {4.3.19}Caso de uso textual - Alterar Dep\IeC {\'o}sito}{64}{subsection.4.3.19}
\contentsline {subsection}{\numberline {4.3.20}Caso de uso textual - Excluir Dep\IeC {\'o}sito}{65}{subsection.4.3.20}
\contentsline {subsection}{\numberline {4.3.21}Caso de uso textual - Pesquisar Dep\IeC {\'o}sito}{66}{subsection.4.3.21}
\contentsline {subsection}{\numberline {4.3.22}Caso de uso textual - Saque Caixa}{67}{subsection.4.3.22}
\contentsline {subsection}{\numberline {4.3.23}Caso de uso textual - Alterar Saque}{68}{subsection.4.3.23}
\contentsline {subsection}{\numberline {4.3.24}Caso de uso textual - Excluir Saque}{69}{subsection.4.3.24}
\contentsline {subsection}{\numberline {4.3.25}Caso de uso textual - Pesquisar Saque}{70}{subsection.4.3.25}
\contentsline {subsection}{\numberline {4.3.26}Caso de uso textual - Gerar Relat\IeC {\'o}rio de Caixa}{71}{subsection.4.3.26}
\contentsline {part}{\partnumberline {III}\MakeTextUppercase {Projeto}}{72}{part.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Projeto de Arquitetura}}{73}{chapter.5}
\contentsline {section}{\numberline {5.1}Descri\IeC {\c c}\IeC {\~a}o}{73}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Diagrama da Arquitetura}{74}{subsection.5.1.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {Projeto de Dados}}{75}{chapter.6}
\contentsline {section}{\numberline {6.1}Diagrama de Pacotes}{75}{section.6.1}
\contentsline {section}{\numberline {6.2}Regras de Neg\IeC {\'o}cio}{77}{section.6.2}
\contentsline {section}{\numberline {6.3}Dicion\IeC {\'a}rio de Dados}{78}{section.6.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {7}\MakeTextUppercase {Prot\IeC {\'o}tipo de interface de alto n\IeC {\'\i }vel}}{87}{chapter.7}
\contentsline {section}{\numberline {7.1}Tela Inicial Login}{87}{section.7.1}
\contentsline {section}{\numberline {7.2}Tela Registrar Usu\IeC {\'a}rio}{88}{section.7.2}
\contentsline {section}{\numberline {7.3}Tela Principal - Menu Rep\IeC {\'u}blica}{89}{section.7.3}
\contentsline {section}{\numberline {7.4}Pesquisa de Rep\IeC {\'u}blica}{90}{section.7.4}
\contentsline {section}{\numberline {7.5}Visualizar Rep\IeC {\'u}blica}{91}{section.7.5}
\contentsline {section}{\numberline {7.6}Cadastro de Rep\IeC {\'u}blica - Sobre a Rep\IeC {\'u}blica}{92}{section.7.6}
\contentsline {section}{\numberline {7.7}Cadastro de Rep\IeC {\'u}blica - Sobre o Im\IeC {\'o}vel}{93}{section.7.7}
\contentsline {section}{\numberline {7.8}Cadastro de Rep\IeC {\'u}blica - Quartos e Restri\IeC {\c c}\IeC {\~o}es}{94}{section.7.8}
\contentsline {section}{\numberline {7.9}Cadastro de Rep\IeC {\'u}blica - Endere\IeC {\c c}o}{95}{section.7.9}
\contentsline {section}{\numberline {7.10}Tela Principal - Financeiro}{96}{section.7.10}
\contentsline {section}{\numberline {7.11}Controle de Despesas - Despesas}{97}{section.7.11}
\contentsline {section}{\numberline {7.12}Controle de Despesas - Cadastro de Despesa}{98}{section.7.12}
\contentsline {section}{\numberline {7.13}Controle de Despesas - Minhas Despesas}{99}{section.7.13}
\contentsline {section}{\numberline {7.14}Controle de Despesas - Relat\IeC {\'o}rio}{100}{section.7.14}
\contentsline {section}{\numberline {7.15}Controle de Caixa - Entradas}{101}{section.7.15}
\contentsline {section}{\numberline {7.16}Controle de Caixa - Dep\IeC {\'o}sito}{102}{section.7.16}
\contentsline {section}{\numberline {7.17}Controle de Caixa - Sa\IeC {\'\i }das}{103}{section.7.17}
\contentsline {section}{\numberline {7.18}Controle de Caixa - Saque}{104}{section.7.18}
\contentsline {section}{\numberline {7.19}Controle de Caixa - Relat\IeC {\'o}rio}{105}{section.7.19}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {8}\MakeTextUppercase {Implementa\IeC {\c c}\IeC {\~a}o}}{106}{chapter.8}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {9}\MakeTextUppercase {Testes e Falhas Conhecidas}}{107}{chapter.9}
\contentsline {section}{\numberline {9.1}Plano de Testes}{107}{section.9.1}
\contentsline {section}{\numberline {9.2}Execu\IeC {\c c}\IeC {\~a}o do Plano de Testes}{107}{section.9.2}
\contentsline {section}{\numberline {9.3}Falhas Conhecidas}{114}{section.9.3}
\contentsline {subsection}{\numberline {9.3.1}Detalhamento}{114}{subsection.9.3.1}
\contentsline {subsection}{\numberline {9.3.2}Prazos previstos para corre\IeC {\c c}\IeC {\~o}es e solu\IeC {\c c}\IeC {\~o}es adotadas}{115}{subsection.9.3.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {10}\MakeTextUppercase {Implanta\IeC {\c c}\IeC {\~a}o}}{116}{chapter.10}
\contentsline {section}{\numberline {10.1}Manual da implanta\IeC {\c c}\IeC {\~a}o}{116}{section.10.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {11}\MakeTextUppercase {Manual do Usu\IeC {\'a}rio}}{117}{chapter.11}
\contentsline {section}{\numberline {11.1}Autentica\IeC {\c c}\IeC {\~a}o}{117}{section.11.1}
\contentsline {section}{\numberline {11.2}Cadastrar rep\IeC {\'u}blica}{125}{section.11.2}
\contentsline {section}{\numberline {11.3}Pesquisar rep\IeC {\'u}blica e solicitar vaga}{133}{section.11.3}
\contentsline {section}{\numberline {11.4}Financeiro - Despesas}{139}{section.11.4}
\contentsline {section}{\numberline {11.5}Financeiro - Caixa}{152}{section.11.5}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {12}\MakeTextUppercase {Conclus\IeC {\~o}es e Considera\IeC {\c c}\IeC {\~o}es Finais}}{160}{chapter.12}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Refer\^encias}{161}{section*.8}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
